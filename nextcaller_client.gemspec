Gem::Specification.new do |s|
  s.name          = 'nextcaller_client'
  s.version       = '0.0.1'
  s.date          = '2014-09-28'
  s.summary       = 'A Ruby wrapper around the Nextcaller API.'
  s.description   = 'A Ruby wrapper around the Nextcaller API.'
  s.authors       = ['Dmitry Vlasov']
  s.email         = 'd.vlasov.work@gmail.com'
  s.files         = %w(Rakefile lib/client.rb)
  s.test_files    = %w(test/test_by_phone.rb test/test_by_profile.rb)
  s.require_paths = ['lib']
  s.homepage      = 'http://rubygems.org/gems/nextcaller_client'
  s.license       = 'MIT'

  s.add_runtime_dependency 'nokogiri'
  s.add_runtime_dependency 'webmock'

  s.required_ruby_version = '>= 1.8.6'
end