require 'base64'

#Basic auth class
class BasicAuth

  def initialize(api_key, api_secret)
    @api_key = api_key
    @api_secret = api_secret
  end

  #Prepare auth_headers
  def get_headers
    value = Base64.encode64(
        ('%s:%s' % [@api_key, @api_secret]).encode('utf-8')
    ).decode('utf-8')
    {Authorization: 'Basic %s' % value}
  end

end