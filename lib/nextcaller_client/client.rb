require_relative './constants'
require_relative './utils'
require_relative './transport'


#The NextCaller API client
class NextcallerClient

  attr_accessor :auth

  def initialize(api_key, api_secret)
    @auth = { username: api_key, password: api_secret }
  end

  # Get profiles by phone
  # arguments:
  #   phone           -- 10 digits phone, str ot int, required
  #   response_format -- response format [json|xml] (default json)
  #   debug           -- boolean (default False)
  #   handler         -- function that will be processing the response.
  #
  def   get_by_phone(phone, response_format=JSON_RESPONSE_FORMAT, debug=false, handler=nil)

    method = 'GET'
    validate_format(response_format)
    validate_phone(phone)
    url_params = {
        phone: phone,
        format: response_format
    }
    url = prepare_url('records', url_params)
    response = make_http_request(@auth, url, method, debug)

  return handler(response) if handler
  default_handle_response(response, response_format)
  end


  # Get profile by id
  # arguments:
  #   profile_id      -- Profile identifier, required
  #   response_format -- response format [json|xml] (default json)
  #   debug           -- boolean (default False)
  #   handler         -- function that will be processing the response.
  #
  def get_by_profile_id(profile_id, response_format=JSON_RESPONSE_FORMAT, debug=false, handler=nil)

    method = 'GET'
    validate_format(response_format)
    url_params = {
        format: response_format
    }
    url = prepare_url('users/%s/' % profile_id, url_params)
    response = make_http_request(@auth, url, method, debug)

    return handler(response) if handler
    default_handle_response(response, response_format)
  end


  # Update profile by id
  # arguments:
  #   profile_id      -- Profile identifier, required
  #   data            -- dictionary with changed data, required
  #   debug           -- boolean (default False)
  #   handler         -- function that will be processing the response.
  #
  def update_by_profile_id(profile_id, data, debug=false, handler=nil)

    method = 'POST'
    url_params = {
        format: JSON_RESPONSE_FORMAT
    }
    url = prepare_url('users/%s/' % profile_id, url_params)
    data = prepare_json_data(data)
    response = make_http_request(@auth, url, method, debug, data)

    return handler(response) if handler
    response
  end

end

