require 'logger'
require 'net/http'
require 'uri'
require 'json'

require_relative './constants'
require_relative './utils'
require_relative './exceptions'

log = Logger.new(STDOUT)
log.level = Logger::DEBUG

def make_http_request(auth, url, method='GET', debug=false, data={}, user_agent=DEFAULT_USER_AGENT,
                      redirect_attempts_left = DEFAULT_REDIRECT_ATTEMPTS)
  raise TooManyRedirects if redirect_attempts_left < 0
  uri = URI.parse(url)
  case method
    when 'GET'
      request = Net::HTTP::Get.new(uri.path)
    when 'POST'
      request = Net::HTTP::Post.new(uri.path)
      request['Content-Type'] = 'application/json'
      request.body = data.to_json
  end
  request.basic_auth auth[:username], auth[:password]
  request['Connection'] = 'Keep-Alive'
  request['User-Agent'] = user_agent if user_agent

  http = Net::HTTP.new(uri.hostname, uri.port)
  http.read_timeout = DEFAULT_REQUEST_TIMEOUT
  http.use_ssl = true
  response = http.start {|http| http.request(request)}

  log.debug('Request url: %s' % url) if debug
  log.debug('Request body: %s' % data.to_s) if debug and method == 'POST'
  case response
    when Net::HTTPSuccess then
      response
    when Net::HTTPRedirection then
      location = response['location']
      log.debug("redirected to: #{location}") if debug
      make_http_request(auth, location, data, method, user_agent, debug, redirect_attempts_left - 1)
    # else
    #   if 400 <= response.code < 500
    #       raise HttpException '%s Client Error: %s' % [response.code, self.reason]
    #   elsif 500 <= response.code < 600
    #     raise HttpException '%s Server Error: %s' % [response.code, self.reason]
    #   end
  end

end