require_relative './constants'
require 'cgi'
require 'json'
require 'nokogiri'

def _handle_json_response(resp)
  JSON.parse(resp)
end

def _handle_xml_response(resp)
  Nokogiri::XML(resp)
end

def prepare_json_data(data)
  begin
    return data.to_json
  rescue ArgumentError, TypeError
    return data
  end
end

def to_json(data)
  begin
    return JSON.parse(data)
  rescue ArgumentError, TypeError
    return data
  end
end

def default_handle_response(resp, response_format='json')
  return _handle_json_response(resp.body) if response_format == 'json'
  return _handle_xml_response(resp.body) if response_format == 'xml'
  resp
end

#Validate phone format
def validate_phone(value, length=DEFAULT_PHONE_LENGTH)
  unless value
    raise ArgumentError, 'Invalid phone number: %s. Phone cannot be blank.' % value
  end
  if value.is_a? Integer
    value = value.to_s
  end
  unless value.is_a? String
    raise ArgumentError, 'Invalid phone number: %s. Phone cannot be type of %s.' % [value, value.class]
  end
  unless value.length == length
    raise ArgumentError, 'Invalid phone number: %s. Phone should has length %s.' % [value, length]
  end
  unless value =~ /^[0-9]+$/
    raise ArgumentError ,'Invalid phone number: %s. Phone should consists of only digits.' % value
  end
end

#Validate response format
def validate_format(response_format)
  unless RESPONSE_FORMATS.include? response_format
    raise ArgumentError, 'Unsupported format: %s. Supported formats are: %s' % [response_format, RESPONSE_FORMATS]
  end
end

#Prepare url from path and params
def prepare_url(path, url_params={})
  url = '%s%s' % [FULL_URL,  path]
  unless url.end_with?('/')
    url += '/'
  end
  unless url_params.empty?
    url_params_str = CGI::escape(url_params.collect{|k,v| "#{k}=#{v}"}.join('&'))
    url += '?' + url_params_str
  end
  url
end

def prepare_url_for_test(auth, path)
  return  Regexp.new 'https://%s:%s@%s%s*' %[auth[:username], auth[:password], BASE_URL, path]
end